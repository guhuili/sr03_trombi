import React from 'react';
import QRCode from 'qrcode.react'
import './App.css';


class Codemail extends React.Component {
  mail = this.props.mail;
  render() {
    //console.log(this.mail)
    return (
      <QRCode
        value={this.mail}  //value参数为生成二维码的链接
        size={100} //二维码的宽高尺寸
        fgColor="#000000"  //二维码的颜色
      />
    );
  }
}

export default Codemail;
