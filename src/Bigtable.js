import React from 'react';
import Codemail from "./Codemail";
import AvatarImage from "./Avatar";
import Codetel from './Codetel';
import './App.css';

import Button from 'react-bootstrap/Button';
import { withStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';


const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },

});

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: '#7da7ad',
    color: theme.palette.common.white,
    fontSize: 20,
    fontWeight: "bold",
  },
  body: {
    fontSize: 13,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
    "&:hover": {
      backgroundColor: "#bdc3c7"
    }
  },
}))(TableRow);

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

class Bigtable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      page: 0,
      rowsPerPage: 10,
      nom_dialog: '',
      mail_dialog: '',
      photo_dialog: '',
      tel_dialog: '',
      fonc_dialog: '',
    };
    this.handleClose = this.handleClose.bind(this);
    this.handleChangePage = this.handleChangePage.bind(this);
    this.handleChangeRowsPerPage = this.handleChangeRowsPerPage.bind(this);
  }

  handleClickOpen(item, e) {
    this.setState({
      open: true,
      nom_dialog: item.nomp,
      mail_dialog: item.mail,
      photo_dialog: item.photo,
      tel_dialog: item.tel,
      fonc_dialog: item.fonction
    })
  };

  handleClose(e) {
    this.setState({ open: false })
  };

  handleChangePage(e, newPage) {
    this.setState({ page: newPage })
  };

  handleChangeRowsPerPage(e) {
    this.setState({ rowsPerPage: parseInt(e.target.value, 10) })
    this.setState({ page: 0 })
  };





  render() {
    const { err, isLoaded, uvs, nom, prenom, lab, foncs } = this.props;
    console.log(uvs)
    //console.log(fonction)
    var list = this.props.uvs.filter(function (item) {
      var flag1 = item.nomAz && item.nomAz.indexOf(nom) !== -1 && item.prenomAz && item.prenomAz.indexOf(prenom) !== -1;
      var flag2 = true
      if (foncs.length > 0) flag2 = item.fonction && foncs.includes(item.fonction.trim().toLowerCase())
      if (lab === "TOUS") {
        return flag1 && flag2;
      }
      else if (!lab) {
        return flag1 && flag2 && !item.structLibelleFils;
      }
      else {
        return item.structLibelleFils && flag1 && flag2 && item.structLibelleFils.indexOf(lab) !== -1;
      }

    })
    //console.log(list)
    if (err) {
      return <div>Erreur : {err.message}</div>;
    } else if (!isLoaded) {
      return <div>Chargement…</div>;
    } else {
      return (
        <div>

          <TableContainer>
            <Table border={1} cellSpacing={"2"}>
              <TableHead >
                <TableRow style={{ backgroundColor: "lightblue" }}>
                  <StyledTableCell> Nom</StyledTableCell>
                  <StyledTableCell> Prenom</StyledTableCell>
                  {/*<th> Email</th>*/}
                  <StyledTableCell> Structure</StyledTableCell>
                  <StyledTableCell> ArbreFils</StyledTableCell>
                  <StyledTableCell> Laboratoire</StyledTableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {list.slice(this.state.page * this.state.rowsPerPage, this.state.page * this.state.rowsPerPage + this.state.rowsPerPage).map(item => (
                  <StyledTableRow key={item.login} onClick={this.handleClickOpen.bind(this, item)} >
                    <StyledTableCell>{item.nomAz}</StyledTableCell>
                    <StyledTableCell>{item.prenomAz}</StyledTableCell>
                    {/*<TableCell>{item.mail}</TableCell>*/}
                    <StyledTableCell>{item.structureAbr}</StyledTableCell>
                    <StyledTableCell>{item.structAbrFils}</StyledTableCell>
                    <StyledTableCell>{item.structLibelleFils}</StyledTableCell>
                  </StyledTableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
          <TablePagination
            rowsPerPageOptions={[10, 20, 30]}
            component="div"
            count={list.length}
            page={this.state.page}
            onChangePage={this.handleChangePage}
            rowsPerPage={this.state.rowsPerPage}
            onChangeRowsPerPage={this.handleChangeRowsPerPage}
          />

          <Dialog onClose={this.handleClose} aria-labelledby="customized-dialog-title" open={this.state.open}>
            <DialogTitle id="customized-dialog-title" onClose={this.handleClose}>{this.state.nom_dialog}</DialogTitle>
            <Divider light />
            <Grid container spacing={3}>
              <Grid item xs={4}>
                <AvatarImage photo={this.state.photo_dialog} />
              </Grid>
              <Grid item xs={4}>
                QR-mail:<br/>
                <Codemail mail={this.state.mail_dialog} />
              </Grid>
              <Grid item xs={4}>
                QR-tel:<br/>
                <Codetel tel={this.state.tel_dialog} />
              </Grid>
              <Grid item xs={12}>
                {this.state.fonc_dialog}
              </Grid>
            </Grid>
            <DialogActions>
              <Button autoFocus onClick={this.handleClose} color="primary">Close</Button>
            </DialogActions>
          </Dialog>
        </div>

      );
    }


  }
}
export default Bigtable;
