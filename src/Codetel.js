import React from 'react';
import QRCode from 'qrcode.react'
import './App.css';

class Codetel extends React.Component {
  render() {
    //console.log(this.tel)
    if (this.tel !== null) {
      return (
        <QRCode
          value={"03 44 23" + this.props.tel}  //value参数为生成二维码的链接
          size={100} //二维码的宽高尺寸
          fgColor="#000000"  //二维码的颜色
        />
      );
    }
    else {
      return (
        <p>Pas de tel</p>
      );
    }
  }
}


export default Codetel;
