import './App.css';
import Bigtable from "./Bigtable";
import React from 'react';
import logo from './utc_logo.png';
import Button from 'react-bootstrap/Button';
import 'bootstrap/dist/css/bootstrap.min.css';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Input from '@material-ui/core/Input';
import MenuItem from '@material-ui/core/MenuItem';




class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      inpValu_nom: '',
      inpValu_prenom: '',
      lab: 'TOUS',
      fonc: [],
      error: null,
      isLoaded: false,
      uvs: [],
    };
    this.handleChange_nom = this.handleChange_nom.bind(this);
    this.handleChange_prenom = this.handleChange_prenom.bind(this);
    this.labChange = this.labChange.bind(this);
    this.foncChange = this.foncChange.bind(this);
  }

  handleChange_nom(e) {
    this.setState({ inpValu_nom: e.target.value })
  }

  handleChange_prenom(e) {
    this.setState({ inpValu_prenom: e.target.value })
  }

  labChange(e) {
    this.setState({ lab: e.target.value })
  }

  foncChange(e) {
    this.setState({ fonc: e.target.value })
  }

  componentDidMount() {//exécuter automatiquement après render()
    fetch('https://webservices.utc.fr/api/v1/trombi/gi', {
      method: 'get',
      headers: new Headers({
        'Authorization': 'Basic ' + btoa('wsuser:v3Kenobi!'),
        'Content-Type': 'application/x-www-form-urlencoded'
      })
    })
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            uvs: result
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }


  render() {
    var foncs = []
    this.state.uvs.forEach(item => {
      if (item.fonction && !foncs.includes(item.fonction.trim().toLowerCase())) {
        foncs.push(item.fonction.trim().toLowerCase())
      }
    })
    const menu = foncs.map((item) => (
      <MenuItem key={item} value={item}>{item}</MenuItem>
    ))
    return (
      <div>
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p> Annuaire des personnels et des doctorants</p>
          <div className="buttongroup">
            <Button variant="success" size="sm">Version PDF</Button>{' '}
            <Button variant="warning" size="sm">Numéros Utiles</Button>{' '}
            <Button variant="danger" href="https://webapplis.utc.fr/webdocuments/ent3/234/organigramme%20UTC-VF%20septembre%202018.pdf" size="sm">Organigramme</Button>{' '}
          </div>
        </header>
        <div style={{
          margin: "3%",
          marginBottom: "0px",
        }}>
          <TextField id="outlined-search" label="Recherche par nom"
            type="search" variant="outlined" onChange={this.handleChange_nom}
            defaultValue={this.state.inpValu_nom}
            size="small" />{' '}
          <TextField id="outlined-search" label="Recherche par prenom"
            type="search" variant="outlined" onChange={this.handleChange_prenom}
            defaultValue={this.state.inpValu_prenom}
            size="small" />{' '}

          <FormControl variant="outlined" size="small">
            <Select native
              value={this.state.lab}
              onChange={this.labChange}
            >
              <option value={"TOUS"}>Tous</option>
              <option value={"heuristique et diagnostic des systèmes complexes"}>Heuristique et diagnostic des systèmes complexes</option>
              <option value={"laboratoire de mathématiques appliquées de Compiègne"}>Laboratoire de mathématiques appliquées de Compiègne</option>
              <option value={"connaissance, organisation et systèmes techniques"}>Connaissance, organisation et systèmes techniques</option>
              <option value={""}> Pas de lab</option>
            </Select>
          </FormControl>
          <br />
          Fonction:
          <FormControl variant="outlined" size="small">
            <Select
              labelId="demo-mutiple-chip-label"
              id="demo-mutiple-chip"
              multiple
              value={this.state.fonc}
              onChange={this.foncChange}
              input={<Input />}
            >
              {menu}
            </Select>
          </FormControl>
        </div>
        <br />
        <div style={{
            margin: "3%",
            marginTop: "5px"
          }}>
            <Bigtable
              err={this.state.error}
              isLoaded={this.state.isLoaded}
              uvs={this.state.uvs}
              nom={this.state.inpValu_nom}
              prenom={this.state.inpValu_prenom}
              lab={this.state.lab}
              foncs={this.state.fonc} />
          </div>
      </div>


    );
  }


}
export default App;
