import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';

const useStyles = theme => ({
  root: {
    display: 'flex',
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  large: {
    width: theme.spacing(15),
    height: theme.spacing(15),
  },
});

class AvatarImage extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Avatar alt="Remy Sharp" className={classes.large} src={`data:image/jpeg;base64,${this.props.photo}`} />
      </div>
    );
  }
}

export default withStyles(useStyles)(AvatarImage)
